;;; personal/russian/ru-dvorak-atreus.el -*- lexical-binding: t; -*-

(require 'quail)
(quail-define-package "ru-dvorak-atreus" "Russian" "RA" t
                  "Russian dvorak-based layout for Keyboardio Atreus that I use"
                  nil t nil nil nil nil nil nil nil nil t)
(seq-mapn (lambda (k v) (quail-defrule (char-to-string k) v))
          "`1234567890[]',.pyfgcrl/=\\aoeuidhtns-;qjkxbmwvz~!@#$%^&*(){}\"<>PYFGCRL?+|AOEUIDHTNS_:QJKXBMWVZäêÄ·"
          "'1234567890хъйцукенгшщзэ=/фывапролдж-ячсмитьбю.~!\"#;%:?*()ХЪЙЦУКЕНГШЩЗЭ+|ФЫВАПРОЛДЖ_ЯЧСМИТЬБЮ,[]{}")
(provide 'ru-dvorak-atreus)
