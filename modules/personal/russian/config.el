;;; -*- lexical-binding: t; -*-

(cond
 ((and (featurep! :personal russion +dvorak)
       (featurep! :personal russian +cmk-dh))
  (progn
    (load! "ru-dvorak")
    (load! "ru-cmk-dh")))
 ((featurep! :personal russian +dvorak)
  (progn
    (load! "ru-dvorak")
    (use-package! ru-dvorak
      :init
      (setq default-input-method 'ru-dvorak))))
 ((featurep! :personal russian +cmk-dh)
  (progn
      (load! "ru-cmk-dh")
      (use-package! ru-cmk
        :init
        (setq default-input-method 'ru-cmk))))
 ((featurep! :personal russian)
  (setq default-input-method 'russian-computer)))

(defun dvatr ()
  (interactive)
  (load! "ru-dvorak-atreus")
  (setq default-input-method 'ru-dvorak-atreus))

;; (when (featurep! :personal russian +dvorak)
;;     (progn
;;       (load! "ru-dvorak")
;;       (use-package! ru-dvorak
;;         :init
;;         (setq default-input-method 'ru-dvorak))))

;; (when (featurep! :personal russian +cmk-dh)
;;     (progn
;;       (load! "ru-cmk-dh")
;;       (use-package! ru-cmk
;;         :init
;;         (setq default-input-method 'ru-cmk))))

;; (when (featurep! :personal russian)
;;   (setq default-input-method 'russian-computer))
